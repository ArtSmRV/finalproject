package com.gmail.sar;

import com.gmail.sar.ErrorType;
import com.gmail.sar.LifeCycle;

public class ErrorDto {
    private String id;
    private String description;
    private String userName;
    private Double swVersion;
    private Integer criticalLevel;
    private ErrorType errorType;
    private LifeCycle lifeCycle;

    public ErrorDto() {
    }

    public ErrorDto(String id, String description, String userName, Double swVersion, Integer criticalLevel, ErrorType errorType, LifeCycle lifeCycle) {
        this.id = id;
        this.description = description;
        this.userName = userName;
        this.swVersion = swVersion;
        this.criticalLevel = criticalLevel;
        this.errorType = errorType;
        this.lifeCycle = lifeCycle;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Double getSwVersion() {
        return swVersion;
    }

    public void setSwVersion(Double swVersion) {
        this.swVersion = swVersion;
    }

    public Integer getCriticalLevel() {
        return criticalLevel;
    }

    public void setCriticalLevel(Integer criticalLevel) {
        this.criticalLevel = criticalLevel;
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    public void setErrorType(ErrorType errorType) {
        this.errorType = errorType;
    }

    public LifeCycle getLifeCycle() {
        return lifeCycle;
    }

    public void setLifeCycle(LifeCycle lifeCycle) {
        this.lifeCycle = lifeCycle;
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", description='" + description + '\'' +
                ", userName='" + userName + '\'' +
                ", swVersion='" + swVersion + '\'' +
                ", criticalLevel='" + criticalLevel + '\'' +
                ", errorType='" + errorType + '\'' +
                ", lifeCycle='" + lifeCycle + '\'' +
                '}';
    }
}
