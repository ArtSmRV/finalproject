package com.gmail.sar;

public enum LifeCycle {
    NEW, APPOINTED, ALLOWED, CLOSED
}
