package com.gmail.sar;

import java.io.*;

public class ErrorService {

    public static void saveError(ErrorDto error) {
        String fileName = Constants.ERROR_MAIN_FOLDER + "/" + error.getUserName() + "/" + error.getId() + ".txt";

        createFolderIfNotPresent(error.getUserName());

        try (PrintWriter pw = new PrintWriter(fileName)) {
            pw.println(error.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void createFolderIfNotPresent(String userName) {
        File dir = new File(Constants.ERROR_MAIN_FOLDER);
        if (!dir.exists()) dir.mkdirs();

        dir = new File(Constants.ERROR_MAIN_FOLDER + "/" + userName);
        if (!dir.exists()) dir.mkdirs();

    }

    public static ErrorDto parseStringToErrorDto(String data) throws CannotParseException {
        ErrorDto errorDto = new ErrorDto();
        errorDto.setId(getStringBetween(data, "id", "description"));
        errorDto.setDescription(getStringBetween(data, "description", "userName"));
        errorDto.setUserName(getStringBetween(data, "userName", "swVersion"));
        errorDto.setSwVersion(Double.parseDouble(getStringBetween(data, "swVersion", "criticalLevel")));
        errorDto.setCriticalLevel(Integer.parseInt(getStringBetween(data, "criticalLevel", "errorType")));
        errorDto.setErrorType(ErrorType.valueOf(getStringBetween(data, "errorType", "lifeCycle")));
        errorDto.setLifeCycle(LifeCycle.valueOf(getLastValue(data, "lifeCycle")));
        return errorDto;
    }

    private static String getStringBetween(String data, String targetField, String nextField) throws CannotParseException {
        String startPart = targetField + "='";
        String endPart = "', " + nextField + "='";
        int startIndex = data.indexOf(startPart) + startPart.length();
        int endIndex = data.indexOf(endPart);
        if (startIndex == (-1 + startPart.length()) || endIndex == -1) {
            throw new CannotParseException("Cannot parse");
        }
        return data.substring(startIndex, endIndex);
    }


    private static String getLastValue(String data, String targetField) throws CannotParseException {
        String startPart = targetField + "='";
        int startIndex = data.indexOf(startPart) + startPart.length();
        int endIndex = data.indexOf("'}");
        if (startIndex == (-1 + startPart.length()) || endIndex == -1) {
            throw new CannotParseException("Cannot parse");
        }
        return data.substring(startIndex, endIndex);
    }


}
