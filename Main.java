package com.gmail.sar;

import java.util.List;
import java.util.Scanner;


public class Main {
    public static void main(String[] args){

        Scanner sc1 = new Scanner(System.in);
        System.out.print("Введите Ваш логин:");
        String name = sc1.nextLine();


        ErrorDto err1 = new ErrorDto("1", "first", "Artur", 1.0, 1, ErrorType.ERROR, LifeCycle.NEW);
        ErrorDto err2 = new ErrorDto("2", "second", "Nick", 1.2, 2, ErrorType.DEFECT, LifeCycle.ALLOWED);
        ErrorDto err3 = new ErrorDto("3", "third", "Mira", 1.5, 10, ErrorType.INTERRUPTION, LifeCycle.CLOSED);
        ErrorDto err4 = new ErrorDto("4", "fourth", "Artur", 2.0, 3, ErrorType.INTERRUPTION, LifeCycle.APPOINTED);
        ErrorDto err5 = new ErrorDto("6", "sdfsdf", "Maria", 5.2, 7, ErrorType.DEFECT, LifeCycle.ALLOWED);

        ErrorService.saveError(err1);
        ErrorService.saveError(err2);
        ErrorService.saveError(err3);
        ErrorService.saveError(err4);
        ErrorService.saveError(err5);


        List<ErrorDto> errorStrings = SearchById.findErrorsByString();
        if (errorStrings.isEmpty()){
            System.out.println("Ошибка с таким Id отсутствует");
        } else {
            errorStrings.forEach(System.out::println);
        }

        List<ErrorDto> errorStrings1 = SearchByErrorType.findErrorsByString1();
        if (errorStrings1.isEmpty()){
            System.out.println("Ошибки с таким ErrorType отсутствуют");
        } else {
            errorStrings1.forEach(System.out::println);
        }

        List<ErrorDto> errorStrings2 = SearchByLifeCycle.findErrorsByString2();
        if (errorStrings2.isEmpty()){
            System.out.println("Ошибки с таким LifeCycle отсутствуют");
        } else {
            errorStrings2.forEach(System.out::println);
        }


    }


}




