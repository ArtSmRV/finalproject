package com.gmail.sar;

public class CannotParseException extends Exception {
    public CannotParseException(String message) {
        super(message);
    }
}

