package com.gmail.sar;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class SearchByErrorType {

    public static List<ErrorDto> findErrorsByString1() {
        Scanner sc1 = new Scanner(System.in);
        System.out.print("Введите ErrorType ошибки(ERROR, DEFECT, INTERRUPTION):");
        String searchString = sc1.nextLine();


        List<File> fileList = null;
        try {
            fileList = Files.walk(Paths.get(Constants.ERROR_MAIN_FOLDER))
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            System.out.println("Warning with dir");
        }


        List<String> errorStrings1 = new ArrayList<>();

        fileList.forEach(file -> {
            try {
                String errorString = Files.readString(file.toPath(), StandardCharsets.UTF_8);
                if (errorString.contains(searchString)) {
                    errorStrings1.add(Files.readString(file.toPath(), StandardCharsets.UTF_8));
                }
            } catch (IOException e) {
                System.out.println("Warning with file");
            }
        });

        List<ErrorDto> res = new ArrayList<>();
        errorStrings1.forEach(errorString -> {
            try {
                res.add(ErrorService.parseStringToErrorDto(errorString));
            } catch (CannotParseException e) {
                e.printStackTrace();
            }
        });
        return res;
    }


}

