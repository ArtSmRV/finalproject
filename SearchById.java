package com.gmail.sar;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class SearchById {

    public static List<ErrorDto> findErrorsByString() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите Id ошибки(пример: id='1'):");
        String searchString = sc.nextLine();


        List<File> fileList = null;
        try {
            fileList = Files.walk(Paths.get(Constants.ERROR_MAIN_FOLDER))
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            System.out.println("Warning with dir");
        }


        List<String> errorStrings = new ArrayList<>();


        fileList.forEach(file -> {
            try {
                String errorString = Files.readString(file.toPath(), StandardCharsets.UTF_8);
                if (errorString.contains(searchString)) {
                    errorStrings.add(Files.readString(file.toPath(), StandardCharsets.UTF_8));
                }
            } catch (IOException e) {
                System.out.println("Warning with file");
            }
        });

        List<ErrorDto> res = new ArrayList<>();
        errorStrings.forEach(errorString -> {
            try {
                res.add(ErrorService.parseStringToErrorDto(errorString));
            } catch (CannotParseException e) {
                e.printStackTrace();
            }
        });
        return res;
    }


}
